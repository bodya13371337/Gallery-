# Gallery Project

A Gallery project created in flutter. Gallery supports mobile, clone the appropriate branches mentioned below:

* For Mobile: https://gitlab.com/bodya13371337/Gallery-/-/tree/main (stable channel)

## Getting Started

The Gallery is simple project that displays random pictures from unsplash.com with the ability to download them.

## How to Use 

**Step 1:**

Download or clone this repo by using the link below:

```
https://gitlab.com/bodya13371337/Gallery-
```

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies: 

```
flutter pub get 
```
